GOLANG SIMILARITY SEARCH
========================
A Golang (GO) implementation of similarity search. 

Author
------
@noronorobeam hadrihilmi[at]gmail[dot]com

Chemoinformatics team, PDCC Research Group, School of Computer Sciences, Universiti Sains Malaysia (USM)
